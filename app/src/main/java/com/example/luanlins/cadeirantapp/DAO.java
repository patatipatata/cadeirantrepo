package com.example.luanlins.cadeirantapp;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Luan Lins.
 */

public class DAO {

    private FirebaseApp app =  FirebaseApp.getInstance();
    private FirebaseDatabase database = FirebaseDatabase.getInstance(app);
    private DatabaseReference pointReference = database.getReference("point");

    public void add(Point p){
        pointReference.child(p.getNome()).setValue(p);

    }

    public void remove(){


    }

    public void update(Point p){
        pointReference.child(p.getNome()).setValue(p);

    }

    public Point getPoint(Point p ){
        return p;
    }
}
