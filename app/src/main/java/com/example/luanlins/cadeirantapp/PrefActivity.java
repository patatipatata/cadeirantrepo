package com.example.luanlins.cadeirantapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class PrefActivity extends AppCompatActivity implements View.OnClickListener{
    private Button botaoOk;
    private Button botaoCancelar;
    private EditText et;
    private CheckBox[] check = new CheckBox[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);
        botaoOk = (Button) findViewById(R.id.button2);
        botaoOk.setOnClickListener(this);
        check[0] = (CheckBox)this.findViewById(R.id.checkBox);
        check[1] = (CheckBox)this.findViewById(R.id.checkBox2);
        check[2] = (CheckBox)this.findViewById(R.id.checkBox3);
        check[3] = (CheckBox)this.findViewById(R.id.checkBox4);
        check[4] = (CheckBox)this.findViewById(R.id.checkBox5);
        et = (EditText)this.findViewById(R.id.editText)
        ;

        botaoCancelar = (Button) findViewById(R.id.button3);
        botaoCancelar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button2){
            Intent it = this.getIntent();
            Bundle b = it.getExtras();
            double[] coordenadas = b.getDoubleArray("location");
            String nome =  et.getText().toString();
            StringBuilder tipo = new StringBuilder();

            for(int i=0; i<5; i++) {
                if (check[i].isChecked()) {
                    tipo.append("|");
                    tipo.append(check[i].getText().toString());
                    tipo.append("|");
                }
            }

            Point p = new Point(nome,tipo.toString(),coordenadas[0],coordenadas[1]);
            DAO dao = new DAO();
            dao.add(p);

            this.finish();

        }else if(v.getId() == R.id.button3){
            this.finish();
        }

    }

}
