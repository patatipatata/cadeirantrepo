package com.example.luanlins.cadeirantapp;

/**
 * Created by Luan Lins.
 */

public class Point {
    double x;
    double y;
    String nome;
    String tipo;

    public Point(String nome,String tipo,double x, double y) {
        this.x = x;
        this.y = y;
        this.nome = nome;
        this.tipo = tipo;
    }
    public Point(){

    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
